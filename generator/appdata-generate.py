#!/usr/bin/python

import os
from ConfigParser import ConfigParser
from fnmatch import fnmatch
from xml.sax.saxutils import escape

def process_desktop_file(f, pkgname, desktopfile):
    config = ConfigParser()
    config.readfp(open(desktopfile))

    id = ('desktop', os.path.basename(desktopfile))
    names = {}
    summaries = {}
    keywords = {}
    icon = None
    appcategories = []
    mimetypes = []
    urls = {}

    items = config.items('Desktop Entry')
    for (key,val) in items:
        if key == 'name':
            names[''] = val
        if key.startswith('name['):
            names[ key[5:-1] ] = val
        if key == 'comment':
            summaries[''] = val
        if key.startswith('comment['):
            summaries[ key[8:-1]] = val
        if key == 'keywords':
            keywords[''] = filter( lambda a: a, val.split(';') )
        if key.startswith('keywords['):
            keywords[ key[9:-1]] = filter( lambda a: a, val.split(';') )
        if key == 'icon':
            icon = ('local', val)
        if key == 'categories':
            appcategories = filter( lambda a: a, val.split(';') )
        if key == 'mimetype':
            mimetypes = filter( lambda a: a, val.split(';') )
        if key == 'homepage':
            urls['homepage'] = val

    f.write('  <application>\n')
    f.write('    <id type="%s">%s</id>\n' % id)
    f.write('    <pkgname>%s</pkgname>\n' % pkgname)
    if names:
        for k in names.keys():
            if k:
                f.write('    <name lang="%s">%s</name>\n' % (k,escape(names[k])))
            else:
                f.write('    <name>%s</name>\n' % escape(names[k]))
    else:
        f.write('    <name></name>\n')
    if summaries:
        for k in summaries.keys():
            if k:
                f.write('    <summary lang="%s">%s</summary>\n' % (k,escape(summaries[k])))
            else:
                f.write('    <summary>%s</summary>\n' % escape(summaries[k]))
    else:
        f.write('    <summary></summary>\n')
    if keywords:
        f.write('    <keywords>\n')
        for k in keywords.keys():
            if k:
                for i in keywords[k]:
                    f.write('      <keyword lang="%s">%s</keyword>\n' % (k,i))
            else:
                for i in keywords[k]:
                    f.write('      <keyword>%s</keyword>\n' % i)
        f.write('    </keywords>\n')
    if icon:
        f.write('    <icon type="%s">%s</icon>\n' % icon)
    else:
        f.write('    <icon type="stock">others</icon>\n')
    f.write('    <appcategories>\n')
    for i in appcategories:
        f.write('      <appcategory>%s</appcategory>\n' % i)
    f.write('    </appcategories>\n')
    if mimetypes:
        f.write('    <mimetypes>\n')
        for i in mimetypes:
            f.write('      <mimetype>%s</mimetype>\n' % i)
        f.write('    </mimetypes>\n')
    for k in urls.keys():
        f.write('    <url type="%s">%s</url>\n' % (k,escape(urls[k])))
    f.write('  </application>\n')


def generate_appdata():
    f = open('appdata.xml', 'w')
    f.write('<?xml version="1.0"?>\n')
    f.write('<applications version="0.1">\n')

    for fn in os.listdir('.'):
        if fnmatch(fn, '*.desktop'):
            pkgname = os.path.splitext(fn)[0]
            process_desktop_file(f, pkgname, fn)

    f.write('</applications>\n')
    f.close()


generate_appdata()
